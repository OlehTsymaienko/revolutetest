package net.lafox.tsyma.revolutTest;

import com.google.inject.AbstractModule;
import net.lafox.tsyma.revolutTest.dao.h2.JdbiProviderH2;
import net.lafox.tsyma.revolutTest.service.MigrationService;
import net.lafox.tsyma.revolutTest.dao.h2.MigrationServiceH2;
import net.lafox.tsyma.revolutTest.dao.AccountDao;
import net.lafox.tsyma.revolutTest.dao.h2.AccountDaoH2;
import net.lafox.tsyma.revolutTest.service.AccountService;
import net.lafox.tsyma.revolutTest.service.AccountServiceImpl;
import org.jdbi.v3.core.Jdbi;

public class AppInjector extends AbstractModule {
    @Override
    protected void configure() {

        //H2 layer
        bind(Jdbi.class).toProvider(JdbiProviderH2.class);
        bind(MigrationService.class).to(MigrationServiceH2.class);
        bind(AccountDao.class).to(AccountDaoH2.class);

        // Service layer
        bind(AccountService.class).to(AccountServiceImpl.class);
    }
}
