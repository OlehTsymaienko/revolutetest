package net.lafox.tsyma.revolutTest;

import static java.lang.Long.parseLong;
import static net.lafox.tsyma.revolutTest.utils.JsonUtil.json;
import static spark.Spark.after;
import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.post;
import static spark.Spark.put;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.lafox.tsyma.revolutTest.service.AccountService;

@Singleton
public class RestController {
    private final AccountService accountService;

    @Inject
    public RestController(AccountService accountService) {
        this.accountService = accountService;
    }

    public void api() {

        get("/ping", (req, res) -> "pong");

        path("/api", () -> {
            get("/account", (req, res) -> accountService.fetchAll(), json());
            get("/account/:id", (req, res) -> accountService.fetch(req.params(":id")), json());
            post("/account/:name", (req, res) -> accountService.create(req.params(":name")), json());
            get("/account/:id/balance", (req, res) -> accountService.balance(req.params(":id")), json());
            post("/account/:id/deposit/:cents", (req, res) -> accountService.deposit(req.params(":id"), parseLong(req.params(":cents"))), json());
            post("/account/:id/withdraw/:cents", (req, res) -> accountService.withdraw(req.params(":id"), parseLong(req.params(":cents"))), json());
            put("/account/:from/:to/transfer/:cents", (req, res) -> accountService.transfer(
                    req.params(":from"),
                    req.params(":to"),
                    parseLong(req.params(":cents"))), json());
        });

        after((req, res) -> res.type("application/json"));

    }
}
