package net.lafox.tsyma.revolutTest.dao.h2;

import javax.inject.Inject;
import javax.inject.Singleton;

import net.lafox.tsyma.revolutTest.service.MigrationService;
import org.jdbi.v3.core.Jdbi;

@Singleton
public class MigrationServiceH2 implements MigrationService {
    private final Jdbi jdbi;

    @Inject
    public MigrationServiceH2(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    @Override
    public void migrate() {// todo: replace by schema migration tool like liqubase or mybatis_migration???

        jdbi.useHandle(handle -> {
            handle.execute("CREATE TABLE IF NOT EXISTS accounts (id VARCHAR PRIMARY KEY, name VARCHAR)");
            handle.execute("CREATE TABLE IF NOT EXISTS cents (id VARCHAR PRIMARY KEY, account_id VARCHAR, count BIGINT)");
        });
    }
}
