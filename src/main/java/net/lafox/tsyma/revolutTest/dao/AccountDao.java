package net.lafox.tsyma.revolutTest.dao;

import java.util.List;

import net.lafox.tsyma.revolutTest.dto.Account;

public interface AccountDao {
    String create(String name);

    List<Account> fetchAll();

    Account fetch(String accountId);

    long balance(String accountId);

    void deposit(String accountId, long cents);

}
