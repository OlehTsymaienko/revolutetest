package net.lafox.tsyma.revolutTest.dao.h2;

import javax.inject.Provider;
import javax.inject.Singleton;

import org.jdbi.v3.core.Jdbi;

@Singleton
public class JdbiProviderH2 implements Provider<Jdbi> {
    private final Jdbi jdbi = Jdbi.create("jdbc:h2:mem:bank;DB_CLOSE_DELAY=-1");

    @Override
    public Jdbi get() {
        return jdbi;
    }
}
