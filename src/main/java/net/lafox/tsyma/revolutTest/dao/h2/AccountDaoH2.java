package net.lafox.tsyma.revolutTest.dao.h2;

import java.util.List;
import java.util.UUID;
import javax.inject.Inject;
import javax.inject.Singleton;

import net.lafox.tsyma.revolutTest.dao.AccountDao;
import net.lafox.tsyma.revolutTest.dto.Account;
import net.lafox.tsyma.revolutTest.exceptions.AccountNotFoundException;
import org.jdbi.v3.core.Jdbi;

@Singleton
public class AccountDaoH2 implements AccountDao {
    private final Jdbi jdbi;

    @Inject
    public AccountDaoH2(Jdbi jdbi) {
        this.jdbi = jdbi;
    }

    @Override
    public String create(String name) {
        return jdbi.withHandle(handle -> {
            String accountId = UUID.randomUUID().toString();
            handle.createUpdate("INSERT INTO accounts (id, name) VALUES(:accountId, :name)")
                    .bind("accountId", accountId)
                    .bind("name", name)
                    .execute();
            return accountId;
        });
    }

    @Override
    public List<Account> fetchAll() {
        return jdbi.withHandle(handle -> handle.createQuery("SELECT * FROM accounts")
                .mapToBean(Account.class)
                .list()
        );
    }

    @Override
    public Account fetch(String accountId) {
        return jdbi.withHandle(handle -> handle.createQuery("SELECT * FROM accounts WHERE id = :accountId")
                .bind("accountId", accountId)
                .mapToBean(Account.class)
                .findOne()
                .orElseThrow(() -> new AccountNotFoundException("Account :" + accountId + " not Found"))
        );
    }

    @Override
    public long balance(String accountId) {
        checkAccount(accountId);
        return jdbi.withHandle(handle -> handle.createQuery("SELECT sum(count) FROM cents WHERE account_id = :accountId")
                .bind("accountId", accountId)
                .mapTo(Long.class)
                .findOne()
                .orElse(0L)
        );
    }

    @Override
    public void deposit(String accountId, long cents) {
        checkAccount(accountId);
        jdbi.withHandle(handle -> handle.createUpdate("INSERT INTO cents (id, account_id, count) VALUES(:id, :accountId, :cents)")
                .bind("id", UUID.randomUUID().toString())
                .bind("accountId", accountId)
                .bind("cents", cents)
                .execute());
    }

    private void checkAccount(String accountId) {
        if (accountId == null) {
            throw new AccountNotFoundException("Account :null not found");
        }
        jdbi.withHandle(handle -> handle.createQuery("SELECT id FROM accounts WHERE id = :accountId")
                .bind("accountId", accountId)
                .mapTo(String.class)
                .findOne()
                .orElseThrow(() -> new AccountNotFoundException("Account :" + accountId + " not found"))
        );
    }

}
