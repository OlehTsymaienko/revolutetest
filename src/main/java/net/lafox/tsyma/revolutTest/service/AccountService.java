package net.lafox.tsyma.revolutTest.service;

import java.util.List;

import net.lafox.tsyma.revolutTest.dto.Account;
import net.lafox.tsyma.revolutTest.dto.Balance;

public interface AccountService {
    Account create(String name);

    List<Account> fetchAll();

    Account fetch(String accountId);

    Balance balance(String accountId);

    Balance deposit(String accountId, long cents);

    Balance withdraw(String accountId, long cents);

    Balance transfer(String accountFrom, String accountTo, long cents);

}
