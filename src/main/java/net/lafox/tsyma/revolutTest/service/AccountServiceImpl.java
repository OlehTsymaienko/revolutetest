package net.lafox.tsyma.revolutTest.service;

import java.util.List;
import javax.inject.Inject;

import net.lafox.tsyma.revolutTest.dao.AccountDao;
import net.lafox.tsyma.revolutTest.dto.Account;
import net.lafox.tsyma.revolutTest.dto.Balance;

public class AccountServiceImpl implements AccountService {
    private final AccountDao accountDao;

    @Inject
    public AccountServiceImpl(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public Account create(String name) {
        String id = accountDao.create(name);
        return accountDao.fetch(id);
    }

    @Override
    public List<Account> fetchAll() {
        return accountDao.fetchAll();
    }

    @Override
    public Account fetch(String accountId) {
        return accountDao.fetch(accountId);
    }

    @Override
    public Balance balance(String accountId) {
        return new Balance(accountDao.balance(accountId));
    }

    @Override
    public Balance deposit(String accountId, long cents) { //todo: verify positive cents
        accountDao.deposit(accountId, cents);
        return balance(accountId);
    }

    @Override
    public Balance withdraw(String accountId, long cents) { //todo: verify positive cents
        accountDao.deposit(accountId, -cents);
        return balance(accountId);
    }

    @Override
    public Balance transfer(String accountIdFrom, String accountIdTo, long cents) {//todo: verify positive cents , verify both accounts before transfer
        withdraw(accountIdFrom, cents);
        try {
            deposit(accountIdTo, cents);
        } catch (Exception e) {
            deposit(accountIdFrom, cents);
            throw new RuntimeException("Transfer " + accountIdFrom + "->" + accountIdTo + " filed", e);
        }
        return balance(accountIdFrom);
    }
}
