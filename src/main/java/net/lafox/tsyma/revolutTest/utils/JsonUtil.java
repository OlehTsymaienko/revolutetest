package net.lafox.tsyma.revolutTest.utils;

import com.google.gson.Gson;
import spark.ResponseTransformer;

public class JsonUtil {

    public static ResponseTransformer json() {
        return object -> new Gson().toJson(object);
    }
}