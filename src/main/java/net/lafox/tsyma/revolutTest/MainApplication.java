package net.lafox.tsyma.revolutTest;

import com.google.inject.Guice;
import com.google.inject.Injector;
import net.lafox.tsyma.revolutTest.service.MigrationService;

public class MainApplication {

    private final Injector injector = Guice.createInjector(new AppInjector());

    public static void main(String[] args) {
        new MainApplication().run();
    }

    private void run() {
        databaseMigration();
        runWebServer();
    }

    private void runWebServer() {
        injector.getInstance(RestController.class).api();
    }

    private void databaseMigration() {
        injector.getInstance(MigrationService.class).migrate();
    }
}
