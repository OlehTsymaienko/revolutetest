package net.lafox.tsyma.revolutTest;


import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static net.lafox.tsyma.revolutTest.DbTestUtils.INJECTOR;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import com.despegar.http.client.GetMethod;
import com.despegar.http.client.HttpClientException;
import com.despegar.http.client.HttpResponse;
import com.despegar.http.client.PostMethod;
import com.despegar.http.client.PutMethod;
import com.despegar.sparkjava.test.SparkServer;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.jayway.jsonpath.JsonPath;
import net.lafox.tsyma.revolutTest.service.MigrationService;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import spark.servlet.SparkApplication;

public class RestControllerTest {
    @ClassRule
    public static SparkServer<RestApp> testServer = new SparkServer<>(RestApp.class, 4568);

    @Before
    public void cleanUpDb() {
        DbTestUtils.cleanup();
    }

    @Test
    public void ping() {
        assertEquals("pong", get("/ping"));
    }

    @Test
    public void accountEmpty() {
        String json = get("/api/account");
        assertThat(json, hasJsonPath("$[*]", Matchers.empty()));
    }

    @Test
    public void accountCreate() {
        String json = post("/api/account/TEST_NAME");
        assertThat(json, hasJsonPath("$.name", equalTo("TEST_NAME")));
        assertThat(json, hasJsonPath("$.id", notNullValue()));

        json = get("/api/account");
        assertThat(json, hasJsonPath("$[*]", Matchers.hasSize(1)));
        assertThat(json, hasJsonPath("$[0].name", equalTo("TEST_NAME")));
        assertThat(json, hasJsonPath("$[0].id", notNullValue()));
    }

    @Test
    public void balance() {
        String json = post("/api/account/TEST_NAME");
        String id = id(json);

        json = get("/api/account/" + id + "/balance");
        assertThat(json, hasJsonPath("$.balance", equalTo(0)));
    }

    @Test
    public void deposit_withdraw() {
        String json = post("/api/account/TEST_NAME");
        String id = id(json);

        json = get("/api/account/" + id + "/balance");
        assertThat(json, hasJsonPath("$.balance", equalTo(0)));

        post("/api/account/" + id + "/deposit/100");
        post("/api/account/" + id + "/withdraw/50");
        post("/api/account/" + id + "/deposit/200");
        post("/api/account/" + id + "/withdraw/1000");
        post("/api/account/" + id + "/deposit/2000");

        json = get("/api/account/" + id + "/balance");
        assertThat(json, hasJsonPath("$.balance", equalTo(1250)));
    }


    @Test
    public void transfer() {
        String json = post("/api/account/TEST_NAME1");
        String id1 = id(json);
        json = post("/api/account/TEST_NAME2");
        String id2 = id(json);


        post("/api/account/" + id1 + "/deposit/100");
        post("/api/account/" + id2 + "/deposit/100");

        put("/api/account/" + id1 + "/" + id2 + "/transfer/50");

        json = get("/api/account/" + id1 + "/balance");
        assertThat(json, hasJsonPath("$.balance", equalTo(50)));

        json = get("/api/account/" + id2 + "/balance");
        assertThat(json, hasJsonPath("$.balance", equalTo(150)));
    }

    private String id(String json) {
        return parse(json, "$.id");
    }

    private String parse(String json, String path) {
        return JsonPath.parse(json).read(path, String.class);
    }


    private String post(String path, String body) {
        return post(path, body, false);
    }

    private String post(String path) {
        return post(path, "", false);
    }

    private String put(String path, String body) {
        return put(path, body, false);
    }

    private String put(String path) {
        return put(path, "", false);
    }

    private String post(String path, String body, boolean followRedirect) {
        try {
            assertNotNull(testServer.getApplication());
            PostMethod post = testServer.post(path, body, followRedirect);
            HttpResponse httpResponse = testServer.execute(post);
            assertEquals(200, httpResponse.code());
            return new String(httpResponse.body());
        } catch (HttpClientException e) {
            throw new RuntimeException(e);
        }
    }

    private String get(String path, boolean followRedirect) {
        try {
            assertNotNull(testServer.getApplication());
            GetMethod get = testServer.get(path, followRedirect);
            HttpResponse httpResponse = null;
            httpResponse = testServer.execute(get);
            assertEquals(200, httpResponse.code());
            return new String(httpResponse.body());
        } catch (HttpClientException e) {
            throw new RuntimeException(e);
        }
    }

    private String put(String path, String body, boolean followRedirect) {
        try {
            assertNotNull(testServer.getApplication());
            PutMethod put = testServer.put(path, body, followRedirect);
            HttpResponse httpResponse = testServer.execute(put);
            assertEquals(200, httpResponse.code());
            return new String(httpResponse.body());
        } catch (HttpClientException e) {
            throw new RuntimeException(e);
        }
    }

    private String get(String path) {
        return get(path, false);
    }

   public static class RestApp implements SparkApplication {
        @Override
        public void init() {
            INJECTOR.getInstance(MigrationService.class).migrate();
            INJECTOR.getInstance(RestController.class).api();
        }
    }


}