package net.lafox.tsyma.revolutTest.dao;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static net.lafox.tsyma.revolutTest.DbTestUtils.INJECTOR;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.HashSet;
import java.util.List;

import net.lafox.tsyma.revolutTest.DbTestUtils;
import net.lafox.tsyma.revolutTest.dto.Account;
import net.lafox.tsyma.revolutTest.exceptions.AccountNotFoundException;
import net.lafox.tsyma.revolutTest.service.MigrationService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class AccountDaoTest {
    private final AccountDao accountDao = INJECTOR.getInstance(AccountDao.class);

    @BeforeAll
    static void databaseMigrate() {
        INJECTOR.getInstance(MigrationService.class).migrate();
    }

    @AfterEach
    void databaseCleanup() {
        DbTestUtils.cleanup();
    }

    @Test
    @DisplayName("create() create One Account")
    void createTest() {
        assertEquals(0, accountDao.fetchAll().size());

        String id = accountDao.create("A");

        assertEquals(1, accountDao.fetchAll().size());
        assertEquals(newAccount(id, "A"), accountDao.fetch(id));
    }

    @Test
    @DisplayName("fetchAll() from empty DB")
    void fetchAllTest1() {
        assertEquals(emptyList(), accountDao.fetchAll());
    }

    @Test
    @DisplayName("fetchAll() with 2 Accounts")
    void fetchAllTest2() {
        String id1 = accountDao.create("A1");
        String id2 = accountDao.create("A2");

        List<Account> accounts = accountDao.fetchAll();
        assertNotNull(accounts);
        assertEquals(2, accounts.size());

        List<Account> expected = asList(
                newAccount(id1, "A1"),
                newAccount(id2, "A2")
        );
        assertEquals(new HashSet<>(expected), new HashSet<>(accounts));
    }

    @Test
    @DisplayName("fetch() real id fetch")
    void fetchTest() {
        String id1 = accountDao.create("A1");
        String id2 = accountDao.create("A2");

        assertEquals(newAccount(id1, "A1"), accountDao.fetch(id1));
        assertEquals(newAccount(id2, "A2"), accountDao.fetch(id2));
    }

    @Test
    @DisplayName("fetch() NULL id fetch")
    void fetchTestNull() {
        assertThrows(AccountNotFoundException.class, () -> {
            accountDao.fetch(null);
        });
    }

    @Test
    @DisplayName("fetch() not existing id fetch")
    void fetchTestNotExistingId() {
        assertThrows(AccountNotFoundException.class, () -> {
            accountDao.fetch("NOT_EXISTING_ID");
        });
    }

    @Test
    @DisplayName("balance() cents for NULL Account")
    void balanceNullAccount() {
        assertThrows(AccountNotFoundException.class, () -> {
            assertEquals(0, accountDao.balance(null));
        });
    }

    @Test
    @DisplayName("balance()  for not existing Account")
    void balanceNotExistingAccount() {
        assertThrows(AccountNotFoundException.class, () -> {
            assertEquals(0, accountDao.balance("NOT_EXISTING_ID"));
        });
    }

    @Test
    @DisplayName("balance() for existing but empty Account")
    void balanceExistingEmptyAccount() {
        String id = accountDao.create("A");
        assertEquals(0, accountDao.balance(id));
    }

    @Test
    @DisplayName("balance() for existing Account with deposit")
    void balanceExistingAccount() {
        String id = accountDao.create("A");
        accountDao.deposit(id, 2);
        accountDao.deposit(id, 3);
        assertEquals(5, accountDao.balance(id));
    }

    @Test
    @DisplayName("deposit() null Account")
    void depositTestNull() {
        assertThrows(AccountNotFoundException.class, () -> {
            accountDao.deposit(null, 2);
        });
    }

    @Test
    @DisplayName("deposit() NOT existing Account")
    void depositTestNotExistingAccount() {
        assertThrows(AccountNotFoundException.class, () -> {
            accountDao.deposit("NOT_EXISTING_ID", 2);
        });
    }

    @Test
    @DisplayName("deposit() existing Account")
    void depositTestExistingAccount() {
        String id1 = accountDao.create("A1");
        String id2 = accountDao.create("A2");
        accountDao.deposit(id1, 2);
        accountDao.deposit(id1, 3);
        accountDao.deposit(id2, -100);
        accountDao.deposit(id1, -50);
        accountDao.deposit(id2, +50);

        assertEquals(-45, accountDao.balance(id1));
        assertEquals(-50, accountDao.balance(id2));
    }


    private Account newAccount(String accountId, String name) {
        return new Account(accountId, name);
    }


}