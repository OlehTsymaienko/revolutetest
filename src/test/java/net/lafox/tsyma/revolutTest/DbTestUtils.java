package net.lafox.tsyma.revolutTest;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.jdbi.v3.core.Jdbi;

public class DbTestUtils {
    public static final Injector INJECTOR = Guice.createInjector(new TestInjector());

    public static void cleanup() {
        Jdbi jdbi = INJECTOR.getInstance(Jdbi.class);
        jdbi.useHandle(handle -> {
            handle.execute("TRUNCATE TABLE cents");
            handle.execute("TRUNCATE TABLE accounts");
        });
    }
}
