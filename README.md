Test task - Design and implement a RESTful API for money transfers between accounts.
### Used Frameworks: 
* Web: Spark Java http://sparkjava.com/
* Database: JDBI  http://jdbi.org/
* IoC: Guice https://github.com/google/guice

### Usage
* build

```$ mvn clean install```

* run

```$ java -jar target/revolutTest-1.0-SNAPSHOT-jar-with-dependencies.jar```

* check

```$ curl http://localhost:4567/api/account```